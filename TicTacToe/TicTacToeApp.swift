//
//  TicTacToeApp.swift
//  TicTacToe
//
//  Created by vikingosegundo on 16/08/2021.
//

import SwiftUI
final
class ViewState: ObservableObject {
    @Published var moves: [Move] = []
    @Published var board: Board = Board(marker: {_ in "" })
    @Published var matchState: Evaluation = .undecided
    
    var game:TicTacToeGame!
    func process(game g:TicTacToeGame) {
        game = g
        board = game.board()
        moves = game.moves()
        matchState = game.state()
    }
}

let viewState = ViewState()
let game = setup(viewState:viewState)

func setup(viewState:ViewState) -> TicTacToeGame {
    var game: TicTacToeGame!
    game = createGame() { _ in viewState.process(game:game) }
    return game
}

@main
struct TicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewState:{ viewState.process(game:game); return viewState }())
        }
    }
}
