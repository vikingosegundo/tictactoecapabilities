//
//  ContentView.swift
//  TicTacToe
//
//  Created by vikingosegundo on 16/08/2021.
//

import SwiftUI

func move(check:(Int, Int), on game:TicTacToeGame) -> Move {
    game.moves().first {
        if case let .set(at: position, execute: _) = $0 {
            return position.0 == check.0
                && position.1 == check.1
        }
     return false
    }!
}

struct ContentView: View {
    init(viewState: ViewState) {
        self.viewState = viewState
    }
    @ObservedObject
    private var viewState: ViewState
    
    func  p(_ player:TicTacToe.Player?) -> String { markerSymbol(for: player) }
    func cc(_ player:TicTacToe.Player?) -> Bool   { player == nil && game.state() == .undecided }
    var body: some View {
        Spacer()
        HStack {
            VStack {
                Button(action:{ cc(viewState.board.data.0.0) ? viewState.game.execute(move(check:(0,0),on:viewState.game)) : () }) { Text(p(viewState.board.data.0.0)) }.frame(minWidth:20).padding()
                Button(action:{ cc(viewState.board.data.0.1) ? viewState.game.execute(move(check:(1,0),on:viewState.game)) : () }) { Text(p(viewState.board.data.0.1)) }.frame(minWidth:20).padding()
                Button(action:{ cc(viewState.board.data.0.2) ? viewState.game.execute(move(check:(2,0),on:viewState.game)) : () }) { Text(p(viewState.board.data.0.2)) }.frame(minWidth:20).padding()
            }
            VStack {
                Button(action:{ cc(viewState.board.data.1.0) ? viewState.game.execute(move(check:(0,1),on:viewState.game)) : () }) { Text(p(viewState.board.data.1.0)) }.frame(minWidth:20).padding()
                Button(action:{ cc(viewState.board.data.1.1) ? viewState.game.execute(move(check:(1,1),on:viewState.game)) : () }) { Text(p(viewState.board.data.1.1)) }.frame(minWidth:20).padding()
                Button(action:{ cc(viewState.board.data.1.2) ? viewState.game.execute(move(check:(2,1),on:viewState.game)) : () }) { Text(p(viewState.board.data.1.2)) }.frame(minWidth:20).padding()
            }
            VStack {
                Button(action:{ cc(viewState.board.data.2.0) ? viewState.game.execute(move(check:(0,2),on:viewState.game)) : () }) { Text(p(viewState.board.data.2.0)) }.frame(minWidth:20).padding()
                Button(action:{ cc(viewState.board.data.2.1) ? viewState.game.execute(move(check:(1,2),on:viewState.game)) : () }) { Text(p(viewState.board.data.2.1)) }.frame(minWidth:20).padding()
                Button(action:{ cc(viewState.board.data.2.2) ? viewState.game.execute(move(check:(2,2),on:viewState.game)) : () }) { Text(p(viewState.board.data.2.2)) }.frame(minWidth:20).padding()
            }
        }
        Spacer()
        Text(matchStateDescription(from:viewState.matchState))
        Text(game.moves().count == 1 ? "(1 possible move)" : "(\(game.moves().count) possible moves)" )
        Spacer()
        Button { viewState.game.execute(.reset) } label: { Text("Reset") }
        Spacer()
    }
    
    func matchStateDescription(from matchState:Evaluation) -> String {
        switch matchState {
        case .undecided   : return " "
        case .draw        : return "It's a tie"
        case let .won(p,m): return "\(m(p)) wins!"
        }
    }
}
