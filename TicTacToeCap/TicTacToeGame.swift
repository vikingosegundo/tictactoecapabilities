//
//  Game.swift
//  Game
//
//  Created by vikingosegundo on 15/08/2021.
//
typealias TicTacToeGame = (
    state:   ()           -> Evaluation, // finished or not
    board:   ()           -> Board,      // access board
    moves:   ()           -> [Move],     // currently available moves
    execute: (Move)       -> (),         // execute move picked form available moves
    print:   ()           -> (),         // print it real good
    callback:(Evaluation) -> ()          // called on board changes
)
enum Move {
    case set(at:(Int,Int),execute:(Board) -> Board)
    case reset
}
enum Evaluation: Equatable, CustomStringConvertible {
    static func == (lhs: Evaluation, rhs: Evaluation) -> Bool { lhs.description == rhs.description }
    var description: String {
        switch self {
        case     .undecided:                    return "undecided"
        case     .draw:                         return "draw"
        case let .won( player, marker: marker): return "Player \( player == .player0 ? "X" : "O" ) (\(marker(player))) won"
        }
    }
    case undecided
    case draw
    case won(Player, marker:(Player?) -> String)
}
func createGame(marker:@escaping (Player?) -> String = markerSymbol(for:), callback: @escaping (Evaluation) -> ()) -> TicTacToeGame {
    var b = Board(marker: marker) { didSet { callback(b.evaluate()) } }
    return (
          state: { b.evaluate()                                            },
          board: { b                                                       },
          moves: { b.possibleMoves(for:b.currentPlayer)                    },
        execute: { switch $0 { case let .set(_,exe): b = exe(b)
                               case     .reset     : b = b.alter(.reset) } },
          print: { b._print()                                              },
        callback: callback
    )
}
enum Player:Equatable {
    case player0
    case player1
    func toggle() -> Player {
        switch self {
        case .player0: return .player1
        case .player1: return .player0 } }
}

func markerSymbol(for player:Player?) -> String {
    switch player {
    case .player0: return "X"
    case .player1: return "O"
    case nil     : return "."
    }
}

struct Board {
    let data: Data
    typealias Data = (
        (Player?, Player?, Player?),
        (Player?, Player?, Player?),
        (Player?, Player?, Player?))
    init(marker: @escaping (Player?) -> String) {
        self.init((
            (nil,nil,nil),
            (nil,nil,nil),
            (nil,nil,nil)),.player0, marker:marker)
    }
    fileprivate enum Change { case set(Player,at:(Int,Int)), reset }
    fileprivate let currentPlayer: Player
    fileprivate let marker       : (Player?) -> String
    fileprivate func evaluate() -> Evaluation {
        (
               (data.0.0 == data.0.1) && (data.0.1 == data.0.2) && (data.0.0 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.1.0 == data.1.1) && (data.1.1 == data.1.2) && (data.1.0 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.2.0 == data.2.1) && (data.2.1 == data.2.2) && (data.2.0 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.0.0 == data.1.0) && (data.1.0 == data.2.0) && (data.0.0 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.0.1 == data.1.1) && (data.1.1 == data.2.1) && (data.0.1 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.0.2 == data.1.2) && (data.1.2 == data.2.2) && (data.0.2 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.0.0 == data.1.1) && (data.1.1 == data.2.2) && (data.0.0 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.0.2 == data.1.1) && (data.1.1 == data.2.0) && (data.0.2 != nil) ? .won(currentPlayer.toggle(), marker:marker)
            :  (data.0.0 != nil) && (data.0.1 != nil) && (data.0.2 != nil)
            && (data.1.0 != nil) && (data.1.1 != nil) && (data.1.2 != nil)
            && (data.2.0 != nil) && (data.2.1 != nil) && (data.2.2 != nil)
            ?  .draw
            :  .undecided)
    }
    private init(_ data: Data, _ currentPlayer:Player, marker: @escaping (Player?) -> String) {
        self.data = data
        self.currentPlayer = currentPlayer
        self.marker = marker
    }
    fileprivate func alter(_ change:Change) -> Self {
        func row(_ row: (Player?, Player?, Player?), with player:Player, at:Int) -> (Player?, Player?, Player?) {
            guard evaluate() == .undecided else { return row }
            switch at {
            case  0: return (player, row.1, row.2)
            case  1: return (row.0, player, row.2)
            case  2: return (row.0, row.1, player)
            default: return row
            }
        }
        switch change {
        case let .set(player,at:(x,y)):
            switch y {
            case  0: return .init((row(data.0,with:player,at:x), data.1,                   data.2                  ), evaluate() == .won(currentPlayer, marker:marker) ? currentPlayer : currentPlayer.toggle(), marker:marker)
            case  1: return .init((    data.0,               row(data.1,with:player,at:x), data.2                  ), evaluate() == .won(currentPlayer, marker:marker) ? currentPlayer : currentPlayer.toggle(), marker:marker)
            case  2: return .init((    data.0,                   data.1,               row(data.2,with:player,at:x)), evaluate() == .won(currentPlayer, marker:marker) ? currentPlayer : currentPlayer.toggle(), marker:marker)
            default: return self
            }
        case .reset: return .init((
            (nil,nil,nil),
            (nil,nil,nil),
            (nil,nil,nil)), .player0, marker:marker)
        }
    }
    private func row(at idx: Int) -> (Player?, Player?, Player?)? {
        switch idx {
        case 0: return data.0
        case 1: return data.1
        case 2: return data.2
        default: return nil
        }
    }
    private func alreadySetPlayer(at idx:Int, from row:(Player?, Player?, Player?)) -> Player? {
        switch idx {
        case 0: return row.0
        case 1: return row.1
        case 2: return row.2
        default: return nil
        }
    }
    fileprivate func possibleMoves(for player:Player) -> [Move] {
        guard evaluate() == .undecided else { return [] }
        let t = createToken()
        return (0..<3).flatMap { i in
            (0..<3).compactMap { j in
                (alreadySetPlayer(at:i,from:row(at:j)!) == nil)
                    && (player == currentPlayer)
                    ? createMove(position:(i,j),token:t)
                    : nil
            }
        }
    }
    fileprivate func _print() {
        print("\(marker(data.0.0)) \(marker(data.0.1)) \(marker(data.0.2))")
        print("\(marker(data.1.0)) \(marker(data.1.1)) \(marker(data.1.2))")
        print("\(marker(data.2.0)) \(marker(data.2.1)) \(marker(data.2.2))")
    }
    private typealias ExclusitivityToken = (redeem:() -> Bool, invalidate:() -> ())
    private func createMove(position:(Int,Int),token:ExclusitivityToken) -> Move {
        .set(at:(position.0,position.1),execute:{
            token.redeem() && ($0.evaluate() == .undecided)
                ? $0.alter(.set($0.currentPlayer,at:position))
                : $0 })
    }
    private func createToken() -> ExclusitivityToken {
        var _token = true
        return (
            redeem    : { defer { _token = false }; return _token },
            invalidate: {         _token = false                  })
    }
}
